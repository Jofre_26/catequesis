/*==============================================================*/
/* Table: AULA                                                  */
/*==============================================================*/
create table AULA (
   ID_AULA              INT4                 not null,
   NUMERO_AULA          INT4                 null,
   constraint PK_AULA primary key (ID_AULA)
);

/*==============================================================*/
/* Table: CATECUMENO                                            */
/*==============================================================*/
create table CATECUMENO (
   ID_CATECUMENO        INT4                 not null,
   ID_AULA              INT4                 not null,
   CEDULA_CATECUMENO    VARCHAR(10)          null,
   NOMBRES_CATECUMENO   VARCHAR(50)          null,
   APELLIDOS_CATECUMENO VARCHAR(50)          null,
   FECHANAC_CATECUMENO  DATE                 null,
   DIRECCION_CATECUMENO VARCHAR(50)          null,
   constraint PK_CATECUMENO primary key (ID_CATECUMENO)
);

/*==============================================================*/
/* Table: CATEQUISTA                                            */
/*==============================================================*/
create table CATEQUISTA (
   ID_CATEQUISTA        INT4                 not null,
   CEDULA_CATEQUISTA    VARCHAR(10)          null,
   NOMBRES_CATEQUISTA   VARCHAR(50)          null,
   APELLIDOS_CATEQUISTA VARCHAR(50)          null,
   DIRECCION_CATEQUISTA VARCHAR(50)          null,
   FECHANAC_CATEQUISTA  DATE                 null,
   TELEFONO_CATEQUISTA  VARCHAR(10)          null,
   constraint PK_CATEQUISTA primary key (ID_CATEQUISTA)
);
/*==============================================================*/
/* Table: MATRICULA                                             */
/*==============================================================*/
create table MATRICULA (
   ID_MATRICULA         INT4                 not null,
   ID_CATECUMENO        INT4                 not null,
   ID_NIVEL             INT4                 not null,
   ID_CATEQUISTA        INT4                 not null,
   ID_REPRESENTANTE     INT4                 not null,
   VALOR_MATRICULA      MONEY                null,
   FECHA_MATRICULA      DATE                 null,
   ESTADO_MATRICULA     VARCHAR(20)          null,
   constraint PK_MATRICULA primary key (ID_MATRICULA)
);

/*==============================================================*/
/* Table: NIVEL                                                 */
/*==============================================================*/
create table NIVEL (
   ID_NIVEL             INT4                 not null,
   NIVEL_NIVEL          VARCHAR(50)          null,
   DESCRIPCION_NIVEL    VARCHAR(50)          null,
   CUPOMIN_NIVEL        INT4                 null,
   CUPOMAX_NIVEL        INT4                 null,
   constraint PK_NIVEL primary key (ID_NIVEL)
);

/*==============================================================*/
/* Table: RELATIONSHIP_7                                        */
/*==============================================================*/
create table RELATIONSHIP_7 (
   ID_AULA              INT4                 not null,
   ID_CATEQUISTA        INT4                 not null,
   constraint PK_RELATIONSHIP_7 primary key (ID_AULA, ID_CATEQUISTA)
);

/*==============================================================*/
/* Table: REPRESENTANTE                                         */
/*==============================================================*/
create table REPRESENTANTE (
   ID_REPRESENTANTE     INT4                 not null,
   CEDULA_REPRESENTANTE VARCHAR(10)          null,
   NOMBRES_REPRESENTANTE VARCHAR(50)          null,
   APELLIDOS_REPRESENTANTE VARCHAR(50)          null,
   DIRECCION_REPRESENTANTE VARCHAR(50)          null,
   TELEFONO_REPRESENTANTE VARCHAR(10)          null,
   VINCULO_REPRESENTANTE VARCHAR(50)          null,
   constraint PK_REPRESENTANTE primary key (ID_REPRESENTANTE)
);

alter table CATECUMENO
   add constraint FK_CATECUME_RELATIONS_AULA foreign key (ID_AULA)
      references AULA (ID_AULA)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_REPRESEN foreign key (ID_REPRESENTANTE)
      references REPRESENTANTE (ID_REPRESENTANTE)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_CATECUME foreign key (ID_CATECUMENO)
      references CATECUMENO (ID_CATECUMENO)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_NIVEL foreign key (ID_NIVEL)
      references NIVEL (ID_NIVEL)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_CATEQUIS foreign key (ID_CATEQUISTA)
      references CATEQUISTA (ID_CATEQUISTA)
      on delete restrict on update restrict;

alter table RELATIONSHIP_7
   add constraint FK_RELATION_RELATIONS_CATEQUIS foreign key (ID_CATEQUISTA)
      references CATEQUISTA (ID_CATEQUISTA)
      on delete restrict on update restrict;

alter table RELATIONSHIP_7
   add constraint FK_RELATION_RELATIONS_AULA foreign key (ID_AULA)
      references AULA (ID_AULA)
      on delete restrict on update restrict;
	  
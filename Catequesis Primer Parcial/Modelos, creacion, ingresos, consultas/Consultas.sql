/*consulta 1 Mostrar a todos los catequistas, debemos saber 
nombres, apellidos y numero de cedula de cada uno que esten en el primer nivel*/
select 
	Catequista.Nombres_Catequista as Nombres,
	Catequista.Apellidos_Catequista as Apellidos,
	Catequista.Cedula_catequista as Cedula,
	Nivel.Nivel_Nivel as Nivel,
	Nivel.Descripcion_Nivel as Subnivel
from Matricula
	inner join Catequista on Catequista.Id_Catequista=Matricula.id_Catequista
	inner join nivel on Nivel.Id_nivel=matricula.id_nivel
where Nivel.Id_nivel=1
group by catequista.Nombres_Catequista,Catequista.Apellidos_Catequista,Catequista.Cedula_catequista,Nivel.Nivel_Nivel,Nivel.Descripcion_Nivel;
/*Consulta 2  Mostrar todos los catecúmenos que estén cursando el ultimo nivel de confirmación, necesitamos saber el nombre,
apellido y fecha de nacimiento de cada uno de los estudiantes.*/
select 
	Catecumeno.Nombres_Catecumeno as Nombre,
	Catecumeno.Apellidos_Catecumeno as Apellido,
	Catecumeno.FechaNac_Catecumeno as Fecha_Nacimiento,
	Nivel.Nivel_Nivel as Nivel,
	Nivel.Descripcion_Nivel as Subnivel
from catecumeno
	inner join Matricula on Catecumeno.Id_catecumeno=Matricula.Id_catecumeno
	inner join nivel on nivel.id_nivel=matricula.Id_nivel
where Nivel.Id_nivel=4
/*Consulta 3 Mostrar los nombres de todos los representantes que tengan a uno de sus 
representados en el primer nivel de primera comunión,
de los cuales necesitamos saber los Apellidos, Nombres y número telefónico*/
select 
	Representante.Nombres_Representante as Nombres,
	Representante.Apellidos_Representante as Apellidos,
	Representante.Telefono_Representante as Telefono,
	Catecumeno.Nombres_Catecumeno as Representado,
	Nivel.Nivel_Nivel as Nivel,
	Nivel.Descripcion_Nivel as Subnivel
from Representante
	inner join Matricula on Matricula.Id_Representante=Representante.Id_Representante 
	inner join catecumeno on Catecumeno.Id_catecumeno=Matricula.Id_catecumeno
	inner join Nivel on Matricula.Id_Nivel=Nivel.Id_Nivel
where Nivel.Id_Nivel=1
/*Consulta 4  Mostrar todo lo recaudado por todas las matriculaciones 
desde que se implementó este nuevo sistema*/
select sum(Valor_Matricula) as Recaudacion_Total from Matricula
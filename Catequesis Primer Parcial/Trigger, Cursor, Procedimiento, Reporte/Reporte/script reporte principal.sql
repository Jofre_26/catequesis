/*Reporte*/
select 
Nivel.descripcion_Nivel, 
Nivel.Id_Nivel, 
Matricula.Estado_Matricula, 
count(Matricula.Estado_Matricula)as cantidad,
extract(year from Matricula.Fecha_Matricula) as periodo
from Matricula
inner join Nivel on Nivel.Id_Nivel=Matricula.Id_Nivel
inner join Catecumeno on Catecumeno.Id_Catecumeno = Matricula.Id_Catecumeno
where Estado_Matricula in ('Progreso','Retirado')
group by 
Nivel.Descripcion_Nivel,
Nivel.Id_Nivel,
Matricula.Estado_Matricula,
periodo
order by
Nivel.Id_Nivel Asc;
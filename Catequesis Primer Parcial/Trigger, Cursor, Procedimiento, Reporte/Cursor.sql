/*se empieza declarando las variables que vamos a usar*/
do $$
declare
Cuantos int=0;
datos Record;
numero Record;
/*Cursor*/
CursorDatos cursor for select * from catequista, nivel, matricula where
Catequista.Id_Catequista=Matricula.Id_Catequista and Nivel.Id_Nivel=Matricula.Id_Nivel and 
Nivel.Id_Nivel= 4 and Nivel.Descripcion_Nivel like '%Con%';
/*Cursor*/
/*Se recorre el cursor*/
begin /*En este caso lo hice con un for para que me contara los registros*/
for numero in CursorDatos loop
cuantos=cuantos+1;
end loop;
open CursorDatos;/*Abrimos el cursor*/
fetch CursorDatos into datos;/*asignamos a una variable tipo record*/
raise notice 'Catequista: %, Nivel: %, Catecumenos: %', Datos.Nombres_Catequista, Datos.Descripcion_Nivel,cuantos;
/*Como en este caso no tenemos mas de un catequista no necesitamos 
recorrerlo mediante un for o un while*/
end $$/*Cerramos*/
language plpgsql;

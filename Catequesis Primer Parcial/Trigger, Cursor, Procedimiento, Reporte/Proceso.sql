/*Proceso*/
create or replace function Ingresos (varchar, integer) returns money
as
$$
select sum(Valor_Matricula) from Matricula, nivel
where
nivel.Id_Nivel=Matricula.Id_Nivel and Descripcion_Nivel= $1 and Nivel.Id_Nivel= $2
$$
language sql;
select Ingresos('1ero de Comunion',1);
select Ingresos('2do de Comunion',2);
select Ingresos('1ero de Confirmacion',3);
select Ingresos('2do de Confirmacion',4);
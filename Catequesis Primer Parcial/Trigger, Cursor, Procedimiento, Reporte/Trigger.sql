 /*Trigger que no permite matricular a un catecumeno en un nivel cuando este ya alcanzo el limite de cupos*/
create or replace function CUPO_MAX() returns trigger/*Se le asigna un nombre*/
as
$CUPO_MAX$
declare /*se declaran las variables que se usaran*/
	cupos_acumulados int;
	tope int;
begin
	select count(*) into cupos_acumulados from matricula where Id_Nivel=new.Id_Nivel; /*aqui asignamos valores que contamos dentro de matricula*/
	select CupoMax_Nivel into tope from Nivel; /*Aqui directamente acogemos el valor que tiene el parametro de CupoMax_nivel que tiene cada nivel*/
	if(cupos_acumulados>=tope)then/*la condicion con sus porcesos.*/
	raise exception 'El cupo de Catecumenos para este nivel ha sido excedido';
	end if;
	return new;
end;
$CUPO_MAX$
language plpgsql;
create trigger CUPO_MAX before insert
on Matricula for each row
execute procedure CUPO_MAX();

insert into Catecumeno values(9,5,3,1,4,'0951734619','Pedro Manuel','Lucas Lucas','18/04/2001','La Lorena');/*ya esta creado*/
insert into Matricula values(9,9,1,1,1,15,'24/10/2021','Progreso');/*al intentar matricularlo se dispara el trigger*/

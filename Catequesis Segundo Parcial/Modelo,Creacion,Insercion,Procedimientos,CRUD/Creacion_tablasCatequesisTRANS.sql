/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     5/1/2022 8:39:10                             */
/*==============================================================*/
objcrud.insertar(txtcedula.getText(),txtnombre.getText(), txtapellido.getText(), txtdireccion.getText(), txtfecha.getText(), txttelefono.getText());

drop table PAGO_MATRICULA;
drop table SACRAMENTO;
drop table MATRICULA;
drop table CATECUMENO;
drop table NIVEL;
drop table RELATIONSHIP_7;
drop table AULA;
drop table CATEQUISTA;
drop table REPRESENTANTE;
drop table TIPO_SACRAMENTO;

set datestyle to 'European';
/*==============================================================*/
/* Table: AULA                                                  */
/*==============================================================*/
create table AULA (
   ID_AULA              INT4                 not null,
   NUMERO_AULA          INT4                 null,
   constraint PK_AULA primary key (ID_AULA)
);

/*==============================================================*/
/* Table: CATECUMENO                                            */
/*==============================================================*/
create table CATECUMENO (
   ID_CATECUMENO        INT4                 not null,
   ID_AULA              INT4                 not null,
   CEDULA_CATECUMENO    VARCHAR(10)          null,
   NOMBRES_CATECUMENO   VARCHAR(50)          null,
   APELLIDOS_CATECUMENO VARCHAR(50)          null,
   FECHANAC_CATECUMENO  DATE                 null,
   DIRECCION_CATECUMENO VARCHAR(50)          null,
   SACRAMENTOAC_CATECUMENO INT4                 null,
   SACAMENTOSIG_CATECUMENO INT4                 null,
   constraint PK_CATECUMENO primary key (ID_CATECUMENO)
);

/*==============================================================*/
/* Table: CATEQUISTA                                            */
/*==============================================================*/
create table CATEQUISTA (
   ID_CATEQUISTA        serial                 not null,
   CEDULA_CATEQUISTA    VARCHAR(10)          null,
   NOMBRES_CATEQUISTA   VARCHAR(50)          null,
   APELLIDOS_CATEQUISTA VARCHAR(50)          null,
   DIRECCION_CATEQUISTA VARCHAR(50)          null,
   FECHANAC_CATEQUISTA  DATE                 null,
   TELEFONO_CATEQUISTA  VARCHAR(10)          null,
   constraint PK_CATEQUISTA primary key (ID_CATEQUISTA)
);

/*==============================================================*/
/* Table: MATRICULA                                             */
/*==============================================================*/
create table MATRICULA (
   ID_MATRICULA         INT4                 not null,
   ID_CATECUMENO        INT4                 not null,
   ID_NIVEL             INT4                 not null,
   ID_CATEQUISTA        INT4                 not null,
   ID_REPRESENTANTE     INT4                 not null,
   VALOR_MATRICULA      MONEY                null,
   FECHA_MATRICULA      DATE                 null,
   ESTADO_MATRICULA     VARCHAR(20)          null,
   RESTANTE_MATRICULA   INT4               null,
   constraint PK_MATRICULA primary key (ID_MATRICULA)
);

/*==============================================================*/
/* Table: NIVEL                                                 */
/*==============================================================*/
create table NIVEL (
   ID_NIVEL             INT4                 not null,
   NIVEL_NIVEL          VARCHAR(50)          null,
   DESCRIPCION_NIVEL    VARCHAR(50)          null,
   CUPOMIN_NIVEL        INT4                 null,
   CUPOMAX_NIVEL        INT4                 null,
   constraint PK_NIVEL primary key (ID_NIVEL)
);

/*==============================================================*/
/* Table: PAGO_MATRICULA                                        */
/*==============================================================*/
create table PAGO_MATRICULA (
   ID_PAGO_MATRICULA    serial                 not null,
   ID_MATRICULA         INT4                 not null,
   FECHA_PAGO_MATRICULA DATE                 null,
   VALOR_PAGO_MATRICULA INT4                null,
   constraint PK_PAGO_MATRICULA primary key (ID_PAGO_MATRICULA)
);

/*==============================================================*/
/* Table: RELATIONSHIP_7                                        */
/*==============================================================*/
create table RELATIONSHIP_7 (
   ID_AULA              INT4                 not null,
   ID_CATEQUISTA        INT4                 not null,
   constraint PK_RELATIONSHIP_7 primary key (ID_AULA, ID_CATEQUISTA)
);

/*==============================================================*/
/* Table: REPRESENTANTE                                         */
/*==============================================================*/
create table REPRESENTANTE (
   ID_REPRESENTANTE     serial                not null,
   CEDULA_REPRESENTANTE VARCHAR(10)          null,
   NOMBRES_REPRESENTANTE VARCHAR(50)          null,
   APELLIDOS_REPRESENTANTE VARCHAR(50)          null,
   DIRECCION_REPRESENTANTE VARCHAR(50)          null,
   TELEFONO_REPRESENTANTE VARCHAR(10)          null,
   VINCULO_REPRESENTANTE VARCHAR(50)          null,
   constraint PK_REPRESENTANTE primary key (ID_REPRESENTANTE)
);

/*==============================================================*/
/* Table: SACRAMENTO                                            */
/*==============================================================*/
create table SACRAMENTO (
   ID_SACRAMENTO        serial                 not null,
   ID_TIPOSACRAMENTO    INT4                 not null,
   ID_CATECUMENO        INT4                 not null,
   ESTADO_SACRAMENTO    VARCHAR(20)          null,
   constraint PK_SACRAMENTO primary key (ID_SACRAMENTO)
);

/*==============================================================*/
/* Table: TIPO_SACRAMENTO                                       */
/*==============================================================*/
create table TIPO_SACRAMENTO (
   ID_TIPOSACRAMENTO    INT4                 not null,
   DESCRIPCION_TIPOSACRAMENTO VARCHAR(20)          null,
   constraint PK_TIPO_SACRAMENTO primary key (ID_TIPOSACRAMENTO)
);

alter table CATECUMENO
   add constraint FK_CATECUME_RELATIONS_AULA foreign key (ID_AULA)
      references AULA (ID_AULA)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_REPRESEN foreign key (ID_REPRESENTANTE)
      references REPRESENTANTE (ID_REPRESENTANTE)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_CATECUME foreign key (ID_CATECUMENO)
      references CATECUMENO (ID_CATECUMENO)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_NIVEL foreign key (ID_NIVEL)
      references NIVEL (ID_NIVEL)
      on delete restrict on update restrict;

alter table MATRICULA
   add constraint FK_MATRICUL_RELATIONS_CATEQUIS foreign key (ID_CATEQUISTA)
      references CATEQUISTA (ID_CATEQUISTA)
      on delete restrict on update restrict;

alter table PAGO_MATRICULA
   add constraint FK_PAGO_MAT_RELATIONS_MATRICUL foreign key (ID_MATRICULA)
      references MATRICULA (ID_MATRICULA)
      on delete restrict on update restrict;

alter table RELATIONSHIP_7
   add constraint FK_RELATION_RELATIONS_CATEQUIS foreign key (ID_CATEQUISTA)
      references CATEQUISTA (ID_CATEQUISTA)
      on delete restrict on update restrict;

alter table RELATIONSHIP_7
   add constraint FK_RELATION_RELATIONS_AULA foreign key (ID_AULA)
      references AULA (ID_AULA)
      on delete restrict on update restrict;

alter table SACRAMENTO
   add constraint FK_SACRAMEN_RELATIONS_TIPO_SAC foreign key (ID_TIPOSACRAMENTO)
      references TIPO_SACRAMENTO (ID_TIPOSACRAMENTO)
      on delete restrict on update restrict;

alter table SACRAMENTO
   add constraint FK_SACRAMEN_RELATIONS_CATECUME foreign key (ID_CATECUMENO)
      references CATECUMENO (ID_CATECUMENO)
      on delete restrict on update restrict;
alter table matricula add check(Restante_matricula>='0');
alter table sacramento add check(id_tiposacramento>='1' and id_tiposacramento<='4');
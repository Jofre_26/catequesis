create or replace procedure transaccion_uno(int,date,int)
as
$$
declare
	x int;
	begin
			insert into pago_matricula(id_matricula,fecha_pago_matricula,valor_pago_matricula) values($1,$2,$3);	
			update matricula set restante_matricula=restante_matricula-$3 where id_matricula=$1;
			select restante_matricula into x from matricula where id_matricula=$1;
			if (x=0) then
				update matricula set estado_matricula='Pagado' where id_matricula=$1;
			end if;
	exception
		when sqlstate '23514' then
		raise exception 'No se realizo la transaccion exedio el pago de la matricula';
		rollback;
	commit;
end;
$$
language plpgsql;

create or replace procedure transaccion_unomal(int,date,int)
as
$$
begin
			insert into pago_matricula(id_matricula,fecha_pago_matricula,valor_pago_matricula) values($1,$2,$3);	
			update matricula set restante_matricula=restante_matricula-$3 where id_matricula=$1;
			update matricula set estado_matricula='Pagado' where id_matricula=$1;
end;
$$
language plpgsql;

select * from pago_matricula;
select * from matricula;
call transaccion_uno(5,'5/10/2021',10)

create or replace procedure transaccion_dos(int,int)
as
$$
declare
	x int;
	y int;
	begin
			insert into sacramento(id_tiposacramento,id_catecumeno,estado_sacramento) values($1,$2,'progreso');	
			select sacamentosig_catecumeno into x from catecumeno where id_catecumeno=$2;
			if($1=x)then
				if(x=4)then
					update catecumeno set sacramentoac_catecumeno=$1,sacamentosig_catecumeno=null where id_catecumeno=$2;
					update sacramento set estado_sacramento='Pasado' where id_catecumeno=$2 and id_tiposacramento=x-1;
				else
					update catecumeno set sacramentoac_catecumeno=$1,sacamentosig_catecumeno=$1+1 where id_catecumeno=$2;
					update sacramento set estado_sacramento='Pasado' where id_catecumeno=$2 and id_tiposacramento=x-1;
				end if;
			else
				raise exception 'No se puede realizar un sacramento de menor rango al actual o saltarse el siguiente rango';
				rollback;
			end if;
	exception
			when sqlstate '23514' then
			raise exception 'No se realizo la transaccion la id del tipo de sacramento no es valida';
			rollback;
	commit;
end;
$$
language plpgsql;

create or replace procedure transaccion_dosmal(int,int)
as
$$
declare
	x int;
	begin
			insert into sacramento(id_tiposacramento,id_catecumeno,estado_sacramento) values($1,$2,'progreso');	
			select sacamentosig_catecumeno into x from catecumeno where id_catecumeno=$2;
			update catecumeno set sacramentoac_catecumeno=$1,sacamentosig_catecumeno=$1+1 where id_catecumeno=$2;
			update sacramento set estado_sacramento='Pasado' where id_catecumeno=$2 and id_tiposacramento=x-1;
end;
$$
language plpgsql;
call transaccion_dos(1,2)
call transaccion_dos(2,2)
call transaccion_dos(3,2)
call transaccion_dos(4,2)

select * from sacramento;
select * from catecumeno;
select * from tipo_sacramento;